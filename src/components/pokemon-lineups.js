import 'antd/dist/antd.css';
import 'antd/dist/antd.less'
import React, { useState, Fragment, useEffect } from 'react';
import {
    Row,
    Col,
    Card,
    Image,
    Button
} from 'antd';

import Teal from '../img/teal.png'
import Red from '../img/red.png'
import Peach from '../img/peach.png'
import Purple from '../img/purple.png'
import Green from '../img/green.png'
import Blue from '../img/blue.png'

import PokemonData from '../components/pokemon-data'

function PokemonLineups(props) {

    const { lineupData } = props
    const [selectPokemon, setSelectPokemon] = useState(null)
    const [selectedData, setSelectedData] = useState({
        id: '',
        name: '',
        type: [],
        body_measures: {
            weight: '',
            height: ''
        },
        stats: {
            hp: '',
            atk: '',
            def: '',
            sp_atk: '',
            sp_def: '',
            speed: ''
        },
        img: ''
    })

    useEffect(() => {
        console.log('selectPokemon', selectPokemon)
        console.log('lineupData', lineupData)
        console.log('selectedData', selectedData)
    }, [selectPokemon, selectedData])

    const clickPokemon = () => {
        if (lineupData.length && lineupData && selectPokemon !== null) {
            setSelectedData({
                id: lineupData[selectPokemon].id,
                name: lineupData[selectPokemon].name,
                type: lineupData[selectPokemon].type.join(', '),
                stats: {
                    hp: lineupData[selectPokemon].stats.hp,
                    atk: lineupData[selectPokemon].stats.atk,
                    def: lineupData[selectPokemon].stats.def,
                    sp_atk: lineupData[selectPokemon].stats.sp_atk,
                    sp_def: lineupData[selectPokemon].stats.sp_def,
                    speed: lineupData[selectPokemon].stats.speed
                }
            })
        }
    }

    return (
        <Fragment>
            <Row>
                <Col>
                    <Card className='card-box-1' title='Pokemon Lineup'>
                        <Row>
                            <Col>
                                <Button
                                    className='card-1'
                                    onClick={() => {
                                        setSelectPokemon(0)
                                        clickPokemon()
                                    }}
                                >
                                    <Image
                                        className='lineup-img'
                                        preview={false}
                                        src={lineupData.length && lineupData[0] &&
                                            lineupData[0].img ? lineupData[0].img : Teal
                                        }
                                    />
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    className='card-2'
                                    onClick={() => {
                                        setSelectPokemon(1)
                                        clickPokemon()
                                    }}
                                >
                                    <Image
                                        className='lineup-img'
                                        preview={false}
                                        src={lineupData.length && lineupData[1] &&
                                            lineupData[1].img ? lineupData[1].img : Red
                                        }
                                    />
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    className='card-3'
                                    onClick={() => {
                                        setSelectPokemon(2)
                                        clickPokemon()
                                    }}
                                >
                                    <Image
                                        className='lineup-img'
                                        preview={false}
                                        src={lineupData.length && lineupData[2] &&
                                            lineupData[2].img ? lineupData[2].img : Peach
                                        }
                                    />
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button
                                    className='card-4'
                                    onClick={() => {
                                        setSelectPokemon(3)
                                        clickPokemon()
                                    }}
                                >
                                    <Image
                                        className='lineup-img'
                                        preview={false}
                                        src={lineupData.length && lineupData[3] &&
                                            lineupData[3].img ? lineupData[3].img : Purple
                                        }
                                    />
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    className='card-5'
                                    onClick={() => {
                                        setSelectPokemon(4)
                                        clickPokemon()
                                    }}
                                >
                                    <Image
                                        className='lineup-img'
                                        preview={false}
                                        src={lineupData.length && lineupData[4] &&
                                            lineupData[4].img ? lineupData[4].img : Green
                                        }
                                    />
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    className='card-6'
                                    onClick={() => {
                                        setSelectPokemon(5)
                                        clickPokemon()
                                    }}
                                >
                                    <Image
                                        className='lineup-img'
                                        preview={false}
                                        src={lineupData.length && lineupData[5] &&
                                            lineupData[5].img ? lineupData[5].img : Blue
                                        }
                                    />
                                </Button>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            <Row>
                <PokemonData
                    selectedData={selectedData}
                />
            </Row>

        </Fragment>
    );
}


export default PokemonLineups;