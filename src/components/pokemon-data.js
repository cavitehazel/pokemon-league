import 'antd/dist/antd.css';
import 'antd/dist/antd.less'
import React, { Fragment } from 'react';
import {
    Row,
    Col,
    Card,
    Empty
} from 'antd';

function PokemonData(props) {
    const { selectedData } = props
    return (
        <Fragment>
            <Card className='card-box-2' title='Pokemon Data'>
                <Card className='card-data' style={{ borderRadius: 20, height: 270 }}>
                    <div className='div-data'>
                        <Row gutter={16, 16} onClick=''>
                            <Col span={8} className='name'>Name</Col>
                            <Col span={8} className='name-content'>
                                {selectedData.name || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16, 22}>
                            <Col span={8} className='type'>
                                Type
                            </Col>
                            <Col span={14} className='type-content'>
                                {selectedData.type || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={8} className='stats'>
                                Stats
                            </Col>
                        </Row>

                        <Row gutter={16, 14, 18}>
                            <Col span={8} />
                            <Col span={6} className='stats-class'>
                                HP
                            </Col>
                            <Col span={10} className='stats-content'>
                                {selectedData.stats.hp || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16, 14, 18}>
                            <Col span={8} />
                            <Col span={6} className='stats-class'>
                                Attack
                            </Col>
                            <Col span={10} className='stats-content'>
                                {selectedData.stats.atk || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16, 14, 18}>
                            <Col span={8} />
                            <Col span={6} className='stats-class'>
                                Defense
                            </Col>
                            <Col span={10} className='stats-content'>
                                {selectedData.stats.def || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16, 14, 18}>
                            <Col span={8} />
                            <Col span={6} className='stats-class'>
                                Sp. Atk
                            </Col>
                            <Col span={10} className='stats-content'>
                                {selectedData.stats.sp_atk || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16, 14, 18}>
                            <Col span={8} />
                            <Col span={6} className='stats-class'>
                                Sp. Def
                            </Col>
                            <Col span={10} className='stats-content'>
                                {selectedData.stats.sp_def || 'N / A'}
                            </Col>
                        </Row>
                        <Row gutter={16, 14, 18}>
                            <Col span={8} />
                            <Col span={6} className='stats-class'>
                                Speed
                            </Col>
                            <Col span={10} className='stats-content'>
                                {selectedData.stats.speed || 'N / A'}
                            </Col>
                        </Row>
                    </div>
                </Card>
            </Card>
        </Fragment>
    );
}


export default PokemonData;