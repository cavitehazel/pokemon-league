import React, {
    useState,
    useEffect
} from 'react'
import {
    Row,
    Col,
    Card,
    Image,
    Input,
    Button,
    Modal,
} from 'antd'
import './App.css'
import _ from 'lodash'
import 'antd/dist/antd.css'
import 'antd/dist/antd.less'
import { getPokemonData, populatePokemonData } from './services/pokemon'
import PokemonLineups from './components/pokemon-lineups'

import {
    PlusCircleOutlined
} from '@ant-design/icons';

const { Search } = Input;

function App() {
    const [pokeData, setPokeData] = useState([])
    const [nextUrl, setNextUrl] = useState('')
    const [prevUrl, setPrevUrl] = useState('')
    const [loading, setLoading] = useState(true)
    const initialUrl = 'https://pokeapi.co/api/v2/pokemon/'
    const [addBtnFlag, setAddBtnFlag] = useState(false)
    const [lineupData, setLineupData] = useState([])
    const [lineupSize, setLineUpSize] = useState(0)
    const [pokedexModal, setPokedexModal] = useState(false)
    const [pokemonDataCard, setPokemonDataCard] = useState(
        {   // Bulbasaur as default data to be shown
            id: '',
            name: '',
            type: [],
            body_measures: {
                weight: '',
                height: ''
            },
            stats: {
                hp: '',
                atk: '',
                def: '',
                sp_atk: '',
                sp_def: '',
                speed: ''
            },
            img: ''
        }
    )

    const defaultPokedexData = {
        id: '1',
        name: 'bulbasaur',
        type: ["grass", "poison"],
        body_measures: {
            weight: '69',
            height: '7'
        },
        stats: {
            hp: '45',
            atk: '49',
            def: '49',
            sp_atk: '65',
            sp_def: '65',
            speed: '45'
        },
        img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg'
    }

    useEffect(() => {
        async function fetchData() {
            let response = await getPokemonData(initialUrl)
            setNextUrl(response.next)
            setPrevUrl(response.previous)
            await populatePokemon(response.results)
            setLoading(false)
        }
        fetchData()
    }, [])

    useEffect(() => {
        addBtnClicked()
    }, [])


    useEffect(() => {
    }, [lineupData, pokedexModal])


    const populatePokemon = async (data) => {
        let pokemonData = await Promise.all(
            data.map(async pokemon => {
                let pokemonRecord = await populatePokemonData(pokemon.url)
                return pokemonRecord
            }))
        setPokeData(pokemonData)
    }

    let hasPokeData = false;
    const onSearch = (value) => {
        _.lowerCase(value)
        let pokeVal = value
        pokeData.map((items) => {
            let typesArr = []
            if (items.name === pokeVal) {
                items.types.map((val) => {
                    typesArr.push(val.type.name)
                })
                setPokemonCard(items, typesArr);
                hasPokeData = true;
            }
        })
        setPokedexModal(!hasPokeData);
    }

    const setPokemonCard = (items, typesArr) => {
        setPokemonDataCard(
            {
                id: items.id,
                name: items.name,
                type: typesArr,
                body_measures: {
                    weight: items.weight,
                    height: items.height
                },
                stats: {
                    hp: items.stats[0].base_stat,
                    atk: items.stats[1].base_stat,
                    def: items.stats[2].base_stat,
                    sp_atk: items.stats[3].base_stat,
                    sp_def: items.stats[4].base_stat,
                    speed: items.stats[5].base_stat
                },
                img: items.sprites.other.dream_world.front_default
            }
        )
    }

    const tokenizeTypes = (typeArr) => {
        let joinedTypes = typeArr.join(', ')
        return joinedTypes
    }

    const addBtnClicked = () => {
        setAddBtnFlag(true)
        getLineupDataSize()
        addPokemon()
    }

    const addBtnClickFalse = () => {
        setAddBtnFlag(false)
    }

    const addPokemon = () => {

        let tempArr = lineupData

        if (addBtnFlag === true) {
            if (pokemonDataCard.id !== '' && lineupSize <= 6) {
                tempArr.push(pokemonDataCard)
            } else {
                tempArr.push(defaultPokedexData)
            }
            setLineupData(tempArr)
            addBtnClickFalse()
        }
    }

    const hideModal = () => {
        setPokedexModal(false)
    }

    const getLineupDataSize = () => {
        setLineUpSize(_.indexOf(lineupData, _.last(lineupData)) + 1)
    }

    return (
        <div className='main-content'>
            <Row className='header-title' align='middle' justify='center'>
                <p className='title'>Pokemon League</p>
            </Row>
            <Modal
                title='Oops!'
                centered
                visible={pokedexModal}
                onOk={hideModal}
                okText="OK"
            >
                <p>Sorry! Pokemon does not exist.</p>
            </Modal>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col flex='800px'>
                    <PokemonLineups
                        lineupData={lineupData}
                        lineupSize={lineupSize}
                    />
                </Col>
                <Col flex='auto'>
                    <Row>
                        <Card className='card-box-3' title='Pokedex'>
                            <Row>
                                <Card style={{ borderRadius: 20, height: 700 }}>
                                    <Search
                                        placeholder='Enter pokemon name'
                                        onSearch={onSearch}
                                        allowClear={true}
                                        style={{ width: 600, marginTop: 30 }}
                                    >
                                    </Search>
                                    <br /><br /><br />
                                    <p className='pokemon-name'>{
                                        `#${pokemonDataCard.id || defaultPokedexData.id} ${pokemonDataCard.name || defaultPokedexData.name}`}
                                    </p>
                                    <span className='pokemon-span-img'>
                                        <Image
                                            className='pokemon-img'
                                            src={pokemonDataCard.img || defaultPokedexData.img}
                                        />
                                    </span>
                                    <br />
                                    <Row gutter={22}>
                                        <Col span={12} className='pokemon-type'>
                                            <span>Type: </span>
                                            <span className='pokemon-type-content'>
                                                {tokenizeTypes(pokemonDataCard.type) || tokenizeTypes(defaultPokedexData.type)}
                                            </span>
                                        </Col>
                                    </Row>
                                    <Row gutter={22}>
                                        <Col span={12} className='pokemon-weight'>
                                            <span>Weight: </span>
                                            <span className='pokemon-weight-content'>
                                                {pokemonDataCard.body_measures.weight || defaultPokedexData.body_measures.weight}
                                            </span>
                                        </Col>
                                    </Row>
                                    <Row gutter={22}>
                                        <Col span={12} className='pokemon-height'>
                                            <span>Height: </span>
                                            <span className='pokemon-height-content'>
                                                {pokemonDataCard.body_measures.height || defaultPokedexData.body_measures.height}
                                            </span>
                                        </Col>
                                    </Row>
                                    <br /><br />
                                    <Button
                                        className='add-btn'
                                        icon={<PlusCircleOutlined />}
                                        size='large'
                                        onClick={addBtnClicked}
                                    >
                                        Add
                                    </Button>
                                    <br /><br />
                                </Card>
                            </Row>
                        </Card>
                    </Row>
                </Col>
            </Row>
        </div>
    );
}

export default App;